import Vue from 'vue'
import Vuex from 'vuex'
import UserModules from './modules/user'
Vue.use(Vuex)
window.mapState = Vuex.mapState;
export default new Vuex.Store({
  modules: { user: UserModules },
  state: {},
  mutations: {},
  actions: {}
})
