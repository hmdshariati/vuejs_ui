export default {
  state: {
    users: {}
  },
  mutations: {
    setInfo (context, data) {
      context.users = data
    }
  },
  actions: {
    getUserInfo (context) {
      window.axios.get('https://jsonplaceholder.typicode.com/users')
        .then(response => {
          context.commit('setInfo', response.data)
        })
    }
  }
}
